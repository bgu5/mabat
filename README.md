# MABAT

MABAT is a python framework for running experiments as part of the `MABAT: A Multi-Armed Bandit Approach for Threat-Hunting` research.

# Technical Details 

## Install

### Linux
MABAT was developed and tested on `Linux` machines. In addition
the `bash` tests scripts under the "experiments\sbatches" are assuming the `sbatch` utility is existing.

### Conda
MABAT is using the `conda` environment manager. The required env is described by the `environment.yml` file in the repository.

Start by installing `conda` on your machine if it's not installed. Conda is available at [Miniconda site](https://docs.conda.io/en/latest/miniconda.html).

After installing conda, environment named "mabat" is created by:
```sh
conda env create -n mabat -f ./environment.yml
```

Next, source the `activate.sh` script to see everything was setup correctly:
```sh
source ./activate.sh

# expected output:
Using 'mabat' conda env.

Available Utils:
 [*] runner - run experiments directly - no sbatch
 [*] mabat_sbatch - run single sbatch
 [*] mabat_multiple_sbatch - run multiple sbatches
 [*] mabatenv - activate mabat conda env
```

## Usage
Everytime you start working with MABAT perfrom:
```sh
source ./activate.sh
``` 
Which will configure the run environment.

In the `activate.sh` script there is a section which can be configured by the user for specific paths. For example the user can configure the conda env name to be used, results dir path and more.

**There are two ways to run tests:**
1. Development environment:

Using the `runner` utility which is exported by `activate.sh`. The runner is receiving as argument the configuration name to run from the configurations exists under "experiments/configs".

For example running localy with the `UCB1 Random` configuration will be:
```sh
# activate mabat conda env if not already activated
mabatenv
# run tests according to supplied configuration
runner --config fp-fn-ucb1-random-with.json
``` 

2. Running multiple tests `SLURM` batches machine:
`sbatch` is a job submitted to run on a high resources computing node.
Submitting `sbatch` can be done using two utilities: `mabat_sbatch` and `mabat_multiple_sbatch`.

* *mabat_sbatch* - submit a single sbatch job running the given tests configuration.
```sh
# n - number of parallel processes to use, c - configuration name, p - prefix to append to results.
mabat_sbatch -n 20 -c fp-fn-egreedy-mmshared-without.json -p aggregated-measures
```

* *mabat_multiple_sbatch* - submit multiple sbatch jobs. The jobs are described by the file "experiments/sbatches/multiple_sbatch.sh". 
**The file should be edited by the user to change the running sbatches.**
```sh
mabat_multiple_sbatch
```

Usually when running multiple different policies with the same configuration the `mabat_multiple_sbatch` will be used.

To update multiple configurations together, for example, change all configuration to `run_type` "rewards" is done using the "experiments/sbatches/update_configs.py" script. More documentation on the script under the "Configuration File" section. 

### Results and Logs
Both the results and logs of tests are saved to the configured results dir - "experiments/results".
The path can be changed by the user in the `activate.sh` file. Run log file is named "runner.log" appended with the creation time.

* If the tests are running localy, that is not as part of an `sbatch` job, the results and logs
will be stored at the path: "experiments/results/$DATE/".

* If the tests are running as part of an `sbatch` job they will be stored at the path: "experiments/results/$DATE/$JOB_ID/".

Under the results dir, the results files are placed in a subdirectory with a name according to the tests `run_type`.

```sh
~/dev/mabat$ ll experiments/results/2022_4_21/
total 60
drwxrwxr-x 6 4096 Apr 21 09:56 ./
drwxrwxr-x 3 4096 Apr 21 08:35 ../
drwxrwxr-x 2 4096 Apr 21 08:35 attacks/
drwxrwxr-x 2 4096 Apr 21 08:35 budget/
drwxrwxr-x 2 4096 Apr 21 09:56 fp-fn/
-rw-rw-r-- 1   22 Apr 21 09:47 fp_fn_merged_EGreedyRandomOrder_rewards_9_47.csv
-rw-rw-r-- 1   20 Apr 21 09:11 fp_fn_merged_ThompsonSamplingRandomOrder_rewards_9_11.csv
-rw-rw-r-- 1   22 Apr 21 09:56 fp_fn_merged_UCB1MostSharedOrder_rewards_9_56.csv
-rw-rw-r-- 1   19 Apr 21 09:38 fp_fn_merged_UCB1RandomOrder_rewards_9_38.csv
-rw-rw-r-- 1   22 Apr 21 09:41 fp_fn_merged_UCB1RandomOrder_rewards_9_41.csv
drwxrwxr-x 2 4096 Apr 21 08:35 place/
-rw-rw-r-- 1 3813 Apr 21 09:11 runner_9_11.log
-rw-rw-r-- 1 4002 Apr 21 09:38 runner_9_38.log
-rw-rw-r-- 1 4009 Apr 21 09:41 runner_9_41.log
-rw-rw-r-- 1 3804 Apr 21 09:47 runner_9_47.log
```
The results files named containing the "merged" text are unified and averaged results of the current run, while the "raw" untouched results are under the other subdirectories (fp-fn, attacks etc). 
Above we can see the results after number of consequent local run, thus we see multiple "runner.log" files. 

Since we are usually using `sbatch` tests, each results and log file are under 
separated dir ("$DATE/$JOB_ID").

**Results file name contains more information about the run:**
```sh
~/dev/mabat$ ll experiments/results/2022_4_21/fp-fn/
total 32
drwxrwxr-x 2 4096 Apr 21 09:56 ./
drwxrwxr-x 6 4096 Apr 21 09:56 ../
-rw-rw-r-- 1   22 Apr 21 09:47 cost_fp_fn_EGreedyRandomOrder_0_rewards1280_19_9_47.csv
-rw-rw-r-- 1   20 Apr 21 09:11 cost_fp_fn_ThompsonSamplingRandomOrder_0_rewards1280_19_9_11.csv
-rw-rw-r-- 1   22 Apr 21 09:56 cost_fp_fn_UCB1MostSharedOrder_0_rewards1280_19_9_56.csv
-rw-rw-r-- 1   22 Apr 21 08:35 cost_fp_fn_UCB1RandomOrder_0_rewards1280_1_8_35.csv
-rw-rw-r-- 1   19 Apr 21 09:38 cost_fp_fn_UCB1RandomOrder_0_rewards1280_19_9_38.csv
-rw-rw-r-- 1   22 Apr 21 09:41 cost_fp_fn_UCB1RandomOrder_0_rewards1280_19_9_41.csv
```

The file name structre is as follows: [RUN_TYPE]-[POLICY_NAME]-[PROCESS_NUM]-[END_CONDITION]-[OCCURING_ATTACK]-[TIME].csv.

So for example for the file **cost_fp_fn_ThompsonSamplingRandomOrder_0_rewards1280_19_9_11.csv** we can understand that:
* It was a *fp-fn* run (test false-positive and false-positive errors).
* The running policy was *ThompsonSampligRandom*.
* The result file was created in process *0* (relevant if multiple parallel are used).
* The tests were running until budget of *1280*.
* The occuring attack id was *19*.

*NOTE:* besides the framework log files the `sbatch` has it's own log file. Their name is "slurm-$JOBID.out" and their are located in the main mabat folder. They might be useful in case a run is failing on the begginig with unexpected reason.

# The Framework Documentation
In this section the framework source code and configuration file will be explained.

## Configuration File
On each run the `runner` is supplied with a `json` configuration file. The file is describing multiple configurations that affect the run. On this section we will go over the meaning of the different configurations and when they are used.

As a reference an example configuration file is given below:
```json
{
    "orders": {
        "UCB1RandomOrder": {}
    },
    "run_params": {
        "host_env_file": "otx-xforce-vt-v22-fixed.p",
        "attack_iterations": 2,
        "budgets": [
                20000
        ],
        "rewards": [
            1280
        ],
        "thresholds": [
                1.0
            ],
        "fp": [0.01],
        "fn": [0.1],
        "niocs_lower_bound": 0,
        "niocs_upper_bound": 50000,
        "create_fp_fn_results": true,
        "create_attacks_results": false,
        "create_budget_results": false,
        "create_rank_results": false,
        "run_type": "rewards",
        "load_pickle_env": true,
        "normalize_results": false,
        "remove_occuring_attack": false,
        "early_occuring_attack_stop": false
                                                                },
    "attacks": {"19":""},
    "credentials": {
        "host": "localhost",
        "port": 11004,
        "user": "neo4j",
        "password": "q1w2e3"
    }
}
```

Let's explain the different keywords:
* `credentials` - the host environment can be built dynamically by connecting to a neo4j server (see "src/host_env.py"). The keyword defines the connection details of the server.

    *NOTE:* The process of building an entire host environment might take a while, depending on the number of artifacts. Thus, instead of loading the same environment multiple times, we have a *nutral environment* that is saved to a file. The environment is saved using the `pickle` python package. The saved host environments are under "experiments/envs".


* `attacks` - a dictionary describing the attacks to be set as the *occuring attack*. The given number is the attack id. Every iteration only a single attack is the *occuring attack*, thus, for each attack in the dict another iteration will be performed. 

    **If the dict is empty each one of the attacks in the knowledge base will be tested**.

* `orders` - a dictionary of dictionarys. The keyword is describing the policies to run with the given configurations. The inner dictionary is used in order to pass specific order params, for example *epsilon* for e-greedy. For each order under `orders` all the tests will run. 

    *NOTE:* as one might seen, we have for each order a separate config file, that is, multiple config files with a single order described. We find this more convinient to run tests parallely on the `slurm` CPUs**.

* **run_params** - configurations specific for the tests run:

    * `host_env_file` - in case the user wished to load a host environment from a file, the name is described here and searched under "experiments/envs".
    * `attack_iterations`- number of iterations for each attack as occuring attack.
    * `budgets` - **configuration specific for budgets run type**. Array of limit budgets to run. For each value an iteration will be performed.
    * `rewards` - **configuration specific for rewards run type**. Array of limit budgets to run. For each value an iteration will be performed.
    * `thresholds` - **configuration specific for thresholds run type**. Array of limit thresholds of found artifacts to reach. For each value an iteration will be performed.
    * `fp` - array of false-positive values. For each combination of `fp` and `fn` an iteration will occur.
    default is *0.01*.
    * `fn` - array of false-negative values. For each combination of `fp` and `fn` an iteration will occur.
    default is *0.1*.
    * `niocs_lower_bound` - limit the min size of attacks participate in investigation (default 0).
    * `niocs_upper_bound` - limit the max size of attacks participate in investigation (default 50000 which is bigger then the biggest attack in our env).
    * `create_fp_fn_results` - boolean, whether to create fp-fn results.
    * `create_attacks_results` - boolean, whether to create attack results.
    * `create_budget_results` - boolean, whether to create budget results.
    * `create_rank_results`: boolean, whether to create rank results.
    * `run_type` - string describing the run type. Options: rewards, budgets, thresholds.
        * `rewards` - run until given budget is reached, that is, number of searches. results are saved under 
        "fp-fn" directory in results dir.
        * `budgets` - similar to `rewards` regarding end condition, but log different results such as `rank` and `confusion matrix` according to configuration. Was used in figure 5.
        results are saved under "budget" directory in results dir.
        * `thresholds` - run until percentage of artifacts are found. Useful when we wan't to this the budget needed until certain threshold.
    * `load_pickle_env` - boolean, whether to load the env described by *host_env_file*.
    * `normalize_results`: boolean, whether to normalize results accoring to `fp` and `fn`.
    * `remove_occuring_attack`: boolean, whether to remove the occuring attack or not before the investigation.
    * `early_occuring_attack_stop`: boolean, whether to stop if found all occuring attack artifacts before reaching certain end condition.

## Order
We call each selection policy an `Order`, since it determines the **search order** of the different artifacts.
All the implemented orders are located under "src/orders/", including an "Abstract Order" (the class name is "Order") which is a base class for all orders.

The goal of Certain order class is to **determine the order of search** to the runner, that is, at each iteration pass the next artifact to search. This is facilitated by the `Order.order()` python generator.

The method `Order.run(...)` is used to run the order, that is, perform the hunting according to the order methodology. Since the search is determined by the `Order.order()` generator, the `Order.run(...)` method is implemented at the base class and using the generator implemented by the inheriting classes.

Lets see an example, below the `RandomOrder` implementation is given. The order is selecting random arm and then selecting random artifact:
```python
class RandomOrder(Order):
    """
    Iterate over all iocs in the graph in random order.
    """

    def __init__(self, host_env, logger, order_params):
        super(RandomOrder, self).__init__(
            host_env, logger, order_params)

    def order(self):
        """
        Generator to determine the order of iteration over objects.
        We say object because it can be only IOCs objects, or attacks pointing
        to IOCs list.
        """
        while True:
            random_attack_id = random.choice(
                list(self.host_env.elements.keys()))
            random_attack_ioc = random.choice(
                list(self.host_env.elements[random_attack_id]))

            if random_attack_ioc.removed:
                # just randomize another one
                continue
            else:
                # let's remove from env and yield
                self.host_env.remove_ioc_from_attack(
                    random_attack_ioc.id, random_attack_id)
                yield random_attack_ioc
```
As we see the `RandomOrder` class is inheriting from the `Order` class.
In addition, only the `order` generator is implemented and the selected artifact is passed using a `yield` statement of python generator.

Lets speak about some of the objects that were used here:

* `host_env` - an object the describe the host environment we are searching on. The `host_env.elements` is a data structure contatins all artifacts in host and their state (search, description etc).
* `logger` - passed in the class constructor, this is the framework logger.
* `order_params` - an object containig the params regarding the order defined in the test configuration file.
This is also logged at the start of every run.


## Algorithms
While the search the order is determined by the different orders classes, algorithms more sophisticated then random are located in a separate directory "src/algorithms". 

Usually an certain order class will used a certain algorithm to determine the search order.

You can find under "algorithms":
* UCB1
* e-Greedy
* Thompson Sampling

## Adding New Order
In order to add a new `order` couple of things need to be done:
1. Implement the order under "src/orders" directory. Don't forget to update the `__init__.py` file there.
2. Have a configuration running the Order you implemented :)

# Results Analysis
On this section we will explain on the results analysis part, including techincal details.
All materials related to the analysis are under the `results_analysis` directory.

## Notebooks
Analysis of the results and figure production was done using `Juyper Notebooks`. The *.ipynb* are supplied under "results_analysis/notebooks" and are devided according to figure types.

The figures output directory is at "results_analysis/figures" and can be modified in the activate.sh script.

### Data Folder
In order to have useful details on the host environment reachable for the analysis number of `csv` file were added under "results_analysis/data-vt-v22".

For example:
* attack_num_iocs_vt_v22 - contains the size of each attack.
* attack_sharing_precentage_vt_v22 - contains the precentage of shared artifact for each attack.

*NOTE:* The path of the data folder can be configured in the activate.sh script.

### Mounting Results
Since the jupyter notebook was unable to run of the `slurm` machine, i used it localy, But the results are on the remote machine. Thus the remote server was mounted using `sshfs` - filesystem over `ssh` to the local PC.

See how to install `sshfs` mount [here](https://phoenixnap.com/kb/sshfs).