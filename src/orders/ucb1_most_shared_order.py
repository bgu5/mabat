import random

from algorithms.ucb1 import UCB1
from .abstract_order import Order


class UCB1MostSharedOrder(Order):
    """
    Order based on UCB algorithm.

    Why Simple ?
    -----------
        Here we assume that all IOCs are from the same type.
        On our origin problem definition every bandit is a
        *type of ioc in attack*. Since here all are from the same type,
        for each attack there is only one bandit.
        Hence when pulling arm, we basically pulling specific malware.
        We translate this to choosing random IOC from same malware and
        checking wether it occurs. In case it does, a reward will be given.
    """

    def __init__(self, host_env, logger, order_params):
        super(UCB1MostSharedOrder, self).__init__(
            host_env, logger, order_params)

    def order(self):
        """
        Generator to determine the order of iteration over objects.
        We say object because it can be only IOCs objects, or attacks pointing
        to IOCs list.
        """
        # number of bandits is as number of attacks
        self.ucb = UCB1(len(self.host_env.elements), self.logger)
        self.items = list(self.host_env.elements.items())
        # shuffle attacks order inplace
        random.shuffle(self.items)

        while True:
            selected_attack_index = self.ucb.select_arm()
            selected_attack_id, _ = self.items[selected_attack_index]
            # get all attack iocs that are *not* removed
            attack_iocs = self.host_env.get_iocs_left(selected_attack_id)

            if not attack_iocs:
                # we checked all iocs of attack
                # hence we put -inf in so it won't be selected anymore
                self.ucb.values[selected_attack_index] = -1e400
                if selected_attack_id == self.host_env.occuring_attack_id:
                    if self.run_params['early_occuring_attack_stop']:
                        self.stop_condition = True
                continue

            # select machine to pull by the number of sharing attacks.
            ioc = self.host_env.get_random_max_shared_ioc(
                selected_attack_id)
            # remove selected ioc from env
            if not self.host_env.remove_ioc_from_attack(
                    ioc.id, selected_attack_id):
                self.logger.warn(
                    f"Failed removng ioc_id={ioc.id} from attack={selected_attack_id}")
            # compute reward for selected attack
            reward = 1 if ioc.occurs else 0
            self.ucb.update(selected_attack_index, reward)
            yield ioc

    def run(self, attack_id, param, run_params):
        norm_cost, _ = super(UCB1MostSharedOrder, self).run(
            attack_id, param, run_params)
        # we are adding another check, specific for UCB algorithms.
        max_ucb_machine_index = self.ucb.get_random_max_ranked_machine()
        occuring_attack_index = get_attack_index(
            self.host_env.occuring_attack_id, self.items)
        result = (occuring_attack_index == max_ucb_machine_index)
        return norm_cost, result


def get_attack_index(requested_attack_id, lst):
    for idx, attack in enumerate(lst):
        attack_id, _ = attack
        if attack_id == requested_attack_id:
            return idx
