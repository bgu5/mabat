import math
import numpy as np
from random import choice


class EGreedy():

    def __init__(self, n_arms, e, logger=None):
        self.e = e
        self.logger = logger

        self.counts = np.zeros((n_arms,), dtype=int)
        # average reward value
        self.values = np.zeros((n_arms,))

    def select_arm(self):
        n_arms = len(self.counts)
        prob = np.random.random()

        if prob <= self.e:
            # explore
            selected_idx = np.random.randint(0, n_arms)
        else:
            # exploit
            selected_idx = self.get_random_max_ranked_machine()

        return selected_idx

    def update(self, chosen_arm, reward, count_step_size=1):
        self.counts[chosen_arm] = self.counts[chosen_arm] + count_step_size
        n = self.counts[chosen_arm]
        value = self.values[chosen_arm]
        new_value = ((n - 1) / float(n)) * value + \
            (1 / float(n)) * reward
        self.values[chosen_arm] = new_value
        return

    def get_random_max_ranked_machine(self):
        max_indice = np.argmax(self.values)
        # get all indices with max value (all machines with max value)
        max_indices = np.where(self.values == self.values[max_indice])[0]
        return choice(max_indices)

    def get_place_for_index(self):
        values = self.values
        values = list(enumerate(values))
        values.sort(key=lambda x: x[1])
        place_for_index = {}
        for idx, t in enumerate(values):
            # create map between the origin index
            # to the new index (place)
            place_for_index[t[0]] = idx
        return place_for_index
