#!/usr/bin/env python
# coding: utf-8

# # Aggregated Measures Figures

# In[1]:


figures_dir = "/home/liadd/research/figures"

def save_fig(fig, name, end="jpg"):
    full_path = f"{figures_dir}/{name}.{end}"
    fig.savefig(full_path)


# In[2]:


import numpy as np
import pandas as pd

from glob import glob
from enum import Enum
from collections import OrderedDict

FREQUENCY = 10
COLUMNS = [0,0.01,0.02,0.04,0.08,0.16,0.32,0.64]
ROWS = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]

num_iocs_df = pd.read_csv("/home/liadd/research/notebook/data-vt-v22/attack_sharing_precentage_vt_v22.csv")

class ConfusionMatrix(Enum):
    RECALL = 0
    PRECISION = 1
    AVG_REWARD = 2

def get_attack_num_iocs(attack_id):
    df = num_iocs_df[num_iocs_df["attack"] == int(attack_id)]
    return int(df["num_iocs"])

def get_attack_details(attack_id):
    df = num_iocs_df[num_iocs_df["attack"] == int(attack_id)]
    return int(df["num_iocs"]), float(df["sharing_precentage"])

def get_attacks_with_num_iocs(niocs_lower_bound=0, niocs_upper_bound=50000):
    df = num_iocs_df[num_iocs_df["num_iocs"] >= niocs_lower_bound]
    df = num_iocs_df[num_iocs_df["num_iocs"] <= niocs_upper_bound]
    return list(df['attack'])

def get_attacks_in_dir(results_dir):
    attack_ids = set()
    for attack_file in glob(f"{results_dir}/cm_*_*.csv*"):
        attack_id = attack_file.split("_")[-3]
        attack_ids.add(attack_id)
    return attack_ids
    
def get_attack_over_time(results_dir, attack_id, iterations, measure, rfp=None, rfn=None, avg_it=2):
    i = 0
    dfs = {}
    attack_num_iocs = -1
    
    # create data frame for each iteration
    while (i <= iterations):
        if rfp == None and rfp == None:
            dfs[i] = pd.DataFrame(columns=COLUMNS, index=ROWS)
        i += FREQUENCY
    
    for attack_file in glob(f"{results_dir}/cm_*_{attack_id}_*_*.csv"):
        attack_file_df = pd.read_csv(attack_file)
        # each attack is a single configuration, i.e. single fp and fn values.
        for r in attack_file_df.iterrows():
            it = r[1]['iteration']
            fp = r[1]['ex_fp']
            fn = r[1]['ex_fn']
            cm_tp, cm_tn, cm_fp, cm_fn = r[1]['cm_tp'],r[1]['cm_tn'],r[1]['cm_fp'],r[1]['cm_fn']
            
            if (it > iterations):
                # sometimes we will ask for less iterations then available
                # thus no need to keep going
                break
                
            if measure == ConfusionMatrix.AVG_REWARD:
                mes = (cm_tp + cm_fp) / float(it)
            elif measure == ConfusionMatrix.RECALL:
                if attack_num_iocs < 0:
                    attack_num_iocs = get_attack_num_iocs(attack_id)
                mes = cm_tp / float(attack_num_iocs)
            elif measure == ConfusionMatrix.PRECISION:
                mes = cm_tp / float(cm_tp + cm_fp)
            
            # set measure in corresponding df and corresponding fp and fn
            if rfp == None and rfn == None:
                try:
                    # already there is a value
                    if not pd.isna(dfs[it][fp][fn]):
                        if type(dfs[it][fp][fn]) is list:
                            dfs[it][fp][fn].append(mes)
                            if len(dfs[it][fp][fn]) == avg_it:
                                # put mean instead of accumulated values
                                dfs[it][fp][fn] = np.array(dfs[it][fp][fn]).mean()
                        else:
                            # create a new list of values
                            dfs[it][fp][fn] = [dfs[it][fp][fn], mes]
                            if len(dfs[it][fp][fn]) == avg_it:
                                # put mean instead of accumulated values
                                dfs[it][fp][fn] = np.array(dfs[it][fp][fn]).mean()
                    else:
                        dfs[it][fp][fn] = mes
                except:
                    pass
            elif (rfp and rfn == None):
                if rfp == fp:
                    pass
            elif (rfp == None and rfn):
                if rfn == fn:
                    pass
            # measure requested in specific fn/fp
            elif (fp == rfp and fn == rfn):
                dfs[it] = mes
    return dfs

def get_attacks_over_time(results_dir, iterations, measure, avg=False, rfp=None, rfn=None):
    attacks_in_dir = get_attacks_in_dir(results_dir)
    attack_results = {}
    
    for attack in attacks_in_dir:
        print(".", end=" ")
        attack_results[attack] = get_attack_over_time(results_dir, attack_id=attack, iterations=iterations,
                                                     measure=measure, rfp=rfp, rfn=rfn)
    temp = []
    if avg:
        if rfp == None and rfn == None:
            for k,v in attack_results.items():
                v_array = list(v.values())
                v_array = np.array([d.to_numpy() for d in v_array])
                
                if len(temp) == 0:
                    temp = v_array
                else:
                    temp += v_array
            return dict(zip(v.keys(), temp / len(attacks_in_dir)))
        else:
            for k,v in attack_results.items():
                v_array = np.array(list(v.values()))
                if len(temp) == 0:
                    temp = v_array
                else:
                    temp += v_array
            return dict(zip(v.keys(), temp / len(attacks_in_dir)))
    else:
        return attack_results
        


# In[172]:


import pandas as pd
import numpy as np
import pylab as pl
import matplotlib.pyplot as plt
import seaborn as sns

plt.style.use('seaborn-whitegrid')

def get_average_measure_all(results_base_num, results_dir, measure, iterations, rfp=0, rfn=0):
    ucb1rnddf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 7}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    rnddf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 3}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    saucbmsdf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 4}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    saeg01msdf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    ucb1msdf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 6}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    rndmsdf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 9}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    eg01rnddf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 2}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    saucbrnddf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 5}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    msdf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 8}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    eg01msdf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 1}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    oracledf = get_attacks_over_time(results_dir=f"{results_dir}/{results_base_num + 10}/fp-fn",
                      iterations=iterations, measure=measure, avg=True, rfp=rfp, rfn=rfn)
    print("")
    
    ret_tuple = (ucb1rnddf, rnddf, saucbmsdf, saeg01msdf, 
                 ucb1msdf, rndmsdf, eg01rnddf, saucbrnddf, 
                 msdf, eg01msdf, oracledf)

    return ret_tuple


def plot_aggregated(results_tuple, xlim=15000, measure="", title="", fp=0, fn=0, yticks=None, fig_path=""):
    (ucb1rnddf, rnddf, saucbmsdf, saeg01msdf, 
     ucb1msdf, rndmsdf, eg01rnddf, saucbrnddf, 
     msdf, eg01msdf, oracledf) = results_tuple
    
    
    fig = plt.figure(figsize=(16,8))
#     fig.set_xlim(0,xlim)
    
    sns.lineplot(list(oracledf.keys()),
                 list(oracledf.values()),
                 linewidth=3, label="ORACLE", color="darkred")

    sns.lineplot(list(saeg01msdf.keys()),
                 list(saeg01msdf.values()),
                 linewidth=3, label="SAEG01-MS", color="tab:blue")

    sns.lineplot(list(saucbmsdf.keys()),
             list(saucbmsdf.values()),
             linewidth=3, label="SAUCB-MS", color="tab:green")

    sns.lineplot(list(eg01msdf.keys()),
             list(eg01msdf.values()),
             linewidth=3, label="EG01-MS", color="tab:orange")

    sns.lineplot(list(ucb1msdf.keys()),
             list(ucb1msdf.values()),
             linewidth=3, label="UCB1-MS", color="tab:red")

    sns.lineplot(list(saucbrnddf.keys()),
             list(saucbrnddf.values()),
             linewidth=3, label="SAUCB-Rnd", color="tab:purple")

    sns.lineplot(list(ucb1rnddf.keys()),
             list(ucb1rnddf.values()),
             linewidth=3, label="UCB1-Rnd", color="tab:pink")

    sns.lineplot(list(eg01rnddf.keys()),
             list(eg01rnddf.values()),
             linewidth=3, label="EG01-Rnd", color="tab:brown")

    sns.lineplot(list(msdf.keys()),
             list(msdf.values()),
             linewidth=3, label="MS", color="tab:olive")

    sns.lineplot(list(rndmsdf.keys()),
             list(rndmsdf.values()),
             linewidth=3, label="Rnd-MS", color="tab:gray")

    sns.lineplot(list(rnddf.keys()),
             list(rnddf.values()),
             linewidth=3, label="Rnd-Rnd",color="tab:cyan")
        
#     fig.suptitle(title, fontweight="bold", fontsize=24)
    plt.xlabel('Iteration', fontsize=30)
    plt.ylabel(measure, fontsize=30)
#     plt.legend(loc="upper right", ncol=3, prop={'size': 24})
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.21),
          ncol=5,  prop={'size': 22})
    
    plt.xticks(fontsize=30)
    plt.yticks([0,0.2,0.4,0.6,0.8,1.0],fontsize=30)
    plt.tight_layout()
    save_fig(fig, fig_path)
                 


# # With Actual Attack

# ### Average Reward

# In[144]:


# with_base_num = 587327
# with_results_dir="/home/liadd/research/results/2021_6_4"
# iterations=15000
# with_average_reward = get_average_measure_all(with_base_num, with_results_dir, 
#                                               measure=ConfusionMatrix.AVG_REWARD, iterations=iterations)


# # In[173]:


# plot_aggregated(with_average_reward, measure="Average Reward", 
#                 title="Average Reward (With actual attack, no noise)",
#                 fig_path="aggregated-measures/average-reward-with-attack-no-noise")


# # ### Precision

# # In[ ]:


# with_precision = get_average_measure_all(with_base_num, with_results_dir, 
#                                               measure=ConfusionMatrix.PRECISION, iterations=iterations)


# # In[ ]:


# plot_aggregated(with_precision, measure="Precision", title="Precision (With actual attack, no noise)",
#                 fig_path="aggregated-measures/precision-with-attack-no-noise")


# # ### Recall

# # In[ ]:



# with_recall = get_average_measure_all(with_base_num, with_results_dir, 
#                                       measure=ConfusionMatrix.RECALL, iterations=iterations)


# # In[133]:


# plot_aggregated(with_recall, 0, measure="Recall", title="Recall (With actual attack, no noise)",
#                 fig_path="aggregated-measures/recall-with-attack-no-noise")


# # # Without actual attack

# # ### Average Reward

# # In[139]:


# without_base_num = 587316
# without_results_dir="/home/liadd/research/results/2021_6_4"
# iterations=15000
# without_average_reward = get_average_measure_all(without_base_num, without_results_dir, 
#                                               measure=ConfusionMatrix.AVG_REWARD, iterations=iterations)


# # In[138]:


# plot_aggregated(without_average_reward, measure="Average Reward", 
#                 title="Average Reward (Without actual attack, no noise)",
#                 fig_path="aggregated-measures/average-reward-without-attack-no-noise")


# # ### Precision

# # In[ ]:


# without_precision = get_average_measure_all(without_base_num, without_results_dir, 
#                                               measure=ConfusionMatrix.PRECISION, iterations=iterations)


# # In[ ]:


# plot_aggregated(without_precision, measure="Precision", title="Precision (without actual attack, no noise)",
#                 fig_path="aggregated-measures/precision-without-attack-no-noise")


# # ### Recall

# # In[ ]:



# without_recall = get_average_measure_all(without_base_num, without_results_dir, 
#                                       measure=ConfusionMatrix.RECALL, iterations=iterations)


# # In[133]:


# plot_aggregated(without_recall, 0, measure="Recall", title="Recall (without actual attack, no noise)",
#                 fig_path="aggregated-measures/recall-without-attack-no-noise")


# # In[ ]:





# # ### Average Reward

# # In[130]:


# with_base_num = 587327
# with_results_dir="/home/liadd/research/results/2021_6_4"
# iterations=15000
# with_average_reward = get_average_measure_all(with_base_num, with_results_dir, 
#                                               measure=ConfusionMatrix.AVG_REWARD, iterations=iterations)


# # In[138]:


# plot_aggregated(with_average_reward, measure="Average Reward", 
#                 title="Average Reward (With actual attack, no noise)",
#                 fig_path="aggregated-measures/average-reward-with-attack-no-noise")


# # ### Precision

# # In[ ]:


# with_precision = get_average_measure_all(with_base_num, with_results_dir, 
#                                               measure=ConfusionMatrix.PRECISION, iterations=iterations)


# # In[ ]:


# plot_aggregated(with_precision, measure="Precision", title="Precision (With actual attack, no noise)",
#                 fig_path="aggregated-measures/precision-with-attack-no-noise")


# # ### Recall

# # In[ ]:



# with_recall = get_average_measure_all(with_base_num, with_results_dir, 
#                                       measure=ConfusionMatrix.RECALL, iterations=iterations)


# # In[133]:


# plot_aggregated(with_recall, 0, measure="Recall", title="Recall (With actual attack, no noise)",
#                 fig_path="aggregated-measures/recall-with-attack-no-noise")


# # # Aggregated Measures Along Sharing Precentage

# # In[22]:


import math
import pandas as pd
import numpy as np


from glob import glob
from pdb import set_trace
from IPython import embed

import math
import pandas as pd
import numpy as np

from collections import OrderedDict
from glob import glob
from pdb import set_trace
from IPython import embed


def is_found_all(attack_id, attack_file):
    num_iocs, sharing_precentage = get_attack_details(attack_id)
    sharing_precentage = round(sharing_precentage, 2)
    df = pd.read_csv(attack_file)
    
    for row in df.iterrows():
        it = row[1]['iteration']
        fp = row[1]['ex_fp']
        fn = row[1]['ex_fn']
        cm_tp, cm_tn, cm_fp, cm_fn = row[1]['cm_tp'],row[1]['cm_tn'],row[1]['cm_fp'],row[1]['cm_fn']
        
        if math.floor(sharing_precentage * num_iocs - (sharing_precentage * num_iocs * fn)) <= cm_tp:
#             print(f"YES - num_iocs={num_iocs}, sp={sharing_precentage}")
            return row
    # in case didn't succeed return the last state.
#     print(f"NO - num_iocs={num_iocs}, sp={sharing_precentage}")
    return row

def get_measure_over_sharing(results_dir, measure, attack_exists=True):
    attacks_in_dir = get_attacks_in_dir(results_dir)
    temp = {}
    df = pd.DataFrame(columns=["sharing_precentage", "measure"])
    
    # iterate over all attacks
    for attack in attacks_in_dir:
        num_iocs, sharing_precentage = get_attack_details(attack)
        print(".", end=" ")
        
        # iterate of all attack files
        for attack_file in glob(f"{results_dir}/cm_*_{attack}_*_*.csv"):
            row = is_found_all(attack, attack_file)
            # if found
            if (row is not None):
                # get measure value
                it = row[1]['iteration']
                fp = row[1]['ex_fp']
                fn = row[1]['ex_fn']
                cm_tp, cm_tn, cm_fp, cm_fn = row[1]['cm_tp'],row[1]['cm_tn'],row[1]['cm_fp'],row[1]['cm_fn']\
                
                if measure == ConfusionMatrix.PRECISION:
                    # TODO: do we used here number of possible of total number of iocs
                    if attack_exists :
                        mes = (cm_tp) / float(math.floor(num_iocs - num_iocs * fn))
                    else:
                        mes = (cm_tp) / float(math.floor(num_iocs*sharing_precentage) - math.floor(num_iocs * sharing_precentage * fn))
                elif measure == ConfusionMatrix.RECALL:
                    if (cm_tp + cm_fp) > 0:
                        mes = float(cm_tp) / (cm_tp + cm_fp) 
                    else:
                        mes = 0
                elif measure == ConfusionMatrix.AVG_REWARD:
                    if it > 0:
                        mes = (cm_tp + cm_fp) / float(it)
                    else:
                        mes = 0
                r = pd.DataFrame.from_dict({"sharing_precentage":[round(sharing_precentage, 2)],
                                            "measure": mes})
                df = df.append(r, ignore_index=True)
                
    try:
        merged_df = df.groupby("sharing_precentage").mean()
    except Exception:
        embed()
    merged_df.reset_index(level=0, inplace=True)
    merged_df = merged_df.sort_values(by=['sharing_precentage'])
    merged_df = merged_df.groupby(np.arange(len(merged_df))//4).mean()
    return (list(merged_df['sharing_precentage']), list(merged_df['measure']))
            
                    
def get_measure_over_sharing_all(results_dir, measure, results_base_num, attack_exists=True):
    ucb1rnddf = get_measure_over_sharing(results_dir=f"{results_dir}/{results_base_num + 7}/fp-fn",
                       measure=measure, attack_exists=attack_exists)
    rnddf = get_measure_over_sharing(results_dir=f"{results_dir}/{results_base_num + 3}/fp-fn",
                       measure=measure, attack_exists=attack_exists)
    saucbmsdf = get_measure_over_sharing(results_dir=f"{results_dir}/{results_base_num + 4}/fp-fn",
                       measure=measure, attack_exists=attack_exists)
    saeg01msdf = get_measure_over_sharing(results_dir=f"{results_dir}/{results_base_num}/fp-fn",
                       measure=measure, attack_exists=attack_exists)
    ucb1msdf = get_measure_over_sharing(results_dir=f"{results_dir}/{results_base_num + 6}/fp-fn",
                       measure=measure, attack_exists=attack_exists)
    rndmsdf = get_measure_over_sharing(results_dir=f"{results_dir}/{results_base_num + 9}/fp-fn",
                       measure=measure, attack_exists=attack_exists)
    eg01rnddf = get_measure_over_sharing(results_dir=f"{results_dir}/{results_base_num + 2}/fp-fn",
                       measure=measure, attack_exists=attack_exists)
    saucbrnddf = get_measure_over_sharing(results_dir=f"{results_dir}/{results_base_num + 5}/fp-fn",
                       measure=measure, attack_exists=attack_exists)
    msdf = get_measure_over_sharing(results_dir=f"{results_dir}/{results_base_num + 8}/fp-fn",
                       measure=measure, attack_exists=attack_exists)
    eg01msdf = get_measure_over_sharing(results_dir=f"{results_dir}/{results_base_num + 1}/fp-fn",
                       measure=measure, attack_exists=attack_exists)
    oracledf = get_measure_over_sharing(results_dir=f"{results_dir}/{results_base_num + 10}/fp-fn",
                       measure=measure, attack_exists=attack_exists)
    print("")
    
    ret_tuple = (ucb1rnddf, rnddf, saucbmsdf, saeg01msdf, 
                 ucb1msdf, rndmsdf, eg01rnddf, saucbrnddf, 
                 msdf, eg01msdf, oracledf)
    
    # ret_tuple = (None, None, saucbmsdf, saeg01msdf, 
    #          None, None, None, None, 
    #          None, None, None)

    return ret_tuple



# In[24]:


import matplotlib.pyplot as plt
import seaborn as sns

plt.style.use('seaborn-whitegrid')

def plot_aggregated_over_sharing(results_tuple, measure, title="", fp=0, fn=0, fig_path="", legend=True):
    (ucb1rnddf, rnddf, saucbmsdf, saeg01msdf, 
     ucb1msdf, rndmsdf, eg01rnddf, saucbrnddf, 
     msdf, eg01msdf, oracledf) = results_tuple
    
    fig = plt.figure(figsize=(14,9))
    
    sns.lineplot(oracledf[0],
                  oracledf[1],
                 linewidth=3.5, label="ORACLE", color="darkred")

    sns.lineplot(saeg01msdf[0],
                  saeg01msdf[1],
                 linewidth=3.5, label="SAEG01-MS", color="tab:blue")

    sns.lineplot(saucbmsdf[0],
              saucbmsdf[1],
             linewidth=3.5, label="SAUCB-MS", color="tab:green")

    sns.lineplot(eg01msdf[0],
              eg01msdf[1],
             linewidth=3.5, label="EG01-MS", color="tab:orange")

    sns.lineplot(ucb1msdf[0],
              ucb1msdf[1],
             linewidth=3.5, label="UCB1-MS", color="tab:red")

    sns.lineplot(saucbrnddf[0],
              saucbrnddf[1],
             linewidth=3.5, label="SAUCB-Rnd", color="tab:purple")

    sns.lineplot(ucb1rnddf[0],
              ucb1rnddf[1],
             linewidth=3.5, label="UCB1-Rnd", color="tab:pink")

    sns.lineplot(eg01rnddf[0],
              eg01rnddf[1],
             linewidth=3.5, label="EG01-Rnd", color="tab:brown")

    sns.lineplot(msdf[0],
              msdf[1],
             linewidth=3.5, label="MS", color="tab:olive")

    sns.lineplot(rndmsdf[0],
              rndmsdf[1],
             linewidth=3.5, label="Rnd-MS", color="tab:gray")

    sns.lineplot(rnddf[0],
              rnddf[1],
             linewidth=3.5, label="Rnd-Rnd", color="tab:cyan")
        
    # fig.suptitle(title, fontweight="bold", fontsize=24)
    plt.xlabel('Shared Artifacts Percentage', fontsize=40)
    plt.ylabel(measure, fontsize=40)
#     plt.legend(loc="upper right", ncol=3, prop={'size': 24})
    # plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.28),
    #       ncol=3,  prop={'size': 26})
    if legend:
        # plt.legend(loc='upper left', ncol=3,  prop={'size': 26})
        plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.40),
          ncol=3,  prop={'size': 26})
    
    plt.xticks(fontsize=40)
    plt.yticks([0,0.2,0.4,0.6,0.8,1.0],fontsize=40)
    plt.tight_layout()
    save_fig(fig, fig_path)


# # No noise

# In[30]:


# with_no_noise_base_num = 587327
# with_no_noise_results_dir="/home/liadd/research/resuelts/2021_6_4"
# with_sharing_results = get_measure_over_sharing_all(with_no_noise_results_dir, ConfusionMatrix.AVG_REWARD, with_no_noise_base_num)
# plot_aggregated_over_sharing(with_sharing_results, measure="Average Reward")


# # In[4]:


# with_no_noise_base_num = 587316
# without_no_noise_results_dir="/home/liadd/research/results/2021_6_4"
# without_sharing_results = get_measure_over_sharing_all(without_no_noise_results_dir, ConfusionMatrix.AVG_REWARD, with_no_noise_base_num)
# plot_aggregated_over_sharing(without_sharing_results, measure="Average Reward")


################################
# DEFAULT NOISE WITH ATTACK    #
################################


with_base_num = 588220
with_results_dir="/home/liadd/research/results/2021_6_5"
with_average_reward_sharing_results = get_measure_over_sharing_all(with_results_dir, 
                                                                   ConfusionMatrix.AVG_REWARD, 
                                                                   with_base_num,
                                                                  attack_exists=True)
plot_aggregated_over_sharing(with_average_reward_sharing_results, measure="Average Reward",
                            fig_path="measures-for-sharing/reward-with-attack-default-noise")


# # # In[18]:


# with_precision_sharing_results = get_measure_over_sharing_all(with_results_dir, 
#                                                               ConfusionMatrix.PRECISION, 
#                                                               with_base_num,
#                                                              attack_exists=True)
# plot_aggregated_over_sharing(with_precision_sharing_results, measure="Precision",
#                              fig_path="measures-for-sharing/precision-with-attack-default-noise-test", legend=False)


# # # In[19]:


# with_recall_sharing_results = get_measure_over_sharing_all(with_results_dir,
#                                                            ConfusionMatrix.RECALL,
#                                                            with_base_num,
#                                                           attack_exists=True)
# plot_aggregated_over_sharing(with_recall_sharing_results, measure="Recall",
#                             fig_path="measures-for-sharing/recall-with-attack-default-noise", legend=False)


################################
# DEFAULT NOISE WITHOUT ATTACK #
################################


# without_base_num = 588231
# without_results_dir="/home/liadd/research/results/2021_6_5"
# without_average_reward_sharing_results = get_measure_over_sharing_all(without_results_dir, 
#                                                                       ConfusionMatrix.AVG_REWARD,
#                                                                       without_base_num,
#                                                                      attack_exists=False)
# plot_aggregated_over_sharing(without_average_reward_sharing_results, measure="Average Reward",
#                             fig_path="measures-for-sharing/reward-without-attack-default-noise")


# # # In[ ]:


# without_precision_sharing_results = get_measure_over_sharing_all(without_results_dir,
#                                                                  ConfusionMatrix.PRECISION,
#                                                                  without_base_num,
#                                                                  attack_exists=False)
# plot_aggregated_over_sharing(without_precision_sharing_results, measure="Precision",
#                              fig_path="measures-for-sharing/precision-without-attack-default-noise", legend=False)


# # # In[ ]:


# without_recall_sharing_results = get_measure_over_sharing_all(without_results_dir,
#                                                               ConfusionMatrix.RECALL,
#                                                               without_base_num,
#                                                               attack_exists=False)
# plot_aggregated_over_sharing(without_recall_sharing_results, measure="Recall",
#                             fig_path="measures-for-sharing/recall-without-attack-default-noise", legend=False)

