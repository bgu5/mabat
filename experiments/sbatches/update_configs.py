import re
import os

from glob import glob

FILES = f"{os.getenv('BASE_DIR')}/experiments/configs/*.json"


def remove_empty_lines(string):
    updated = filter(None, string.splitlines())
    updated = "\n".join(updated)
    updated = filter(str.strip, updated.splitlines())
    updated = "\n".join(updated)
    return updated


def field_update(find, repl, path):
    with open(path, "r") as f:
        content = f.read()

    updated = re.sub(find, repl, content, flags=re.DOTALL)
    updated = remove_empty_lines(updated)

    with open(path, "w") as f:
        f.write(updated)


def update_attacks(path):
    NEW_ATTACKS = \
        r"""
    "attacks": {"19":""},
    """
    PATTERN = r'\"attacks\": {.*?},'
    field_update(PATTERN, NEW_ATTACKS, path)


def update_fp_fn(path):
    NEW_FP = \
        r"""
        "fp": [0.01],
    """
    FP_PATTERN = r'\"fp\": \[.*?\],'

    NEW_FN = \
        r"""
        "fn": [0.1],
    """
    FN_PATTERN = r'\"fn\": \[.*?\],'

    field_update(FP_PATTERN, NEW_FP, path)
    field_update(FN_PATTERN, NEW_FN, path)


def update_thresholds(path):
    NEW_THRESHOLDS = \
        r"""
        "thresholds": [
                1.0
            ],
    """
    PATTERN = r'\"thresholds\": \[.*?\],'

    field_update(PATTERN, NEW_THRESHOLDS, path)


def update_budgets(path):
    NEW_THRESHOLDS = \
        r"""
        "budgets": [
                20000
        ],
    """
    PATTERN = r'\"budgets\": \[.*?\],'

    field_update(PATTERN, NEW_THRESHOLDS, path)


def update_run_type(path):
    NEW_RUNTYPE = \
        r"""
        "run_type": "rewards",
    """
    PATTERN = r'\"run_type\": .*?,'

    field_update(PATTERN, NEW_RUNTYPE, path)


def update_remove_occuring_attack(path):
    NEW_REMOVE_OCCURING = \
        r"""
        "remove_occuring_attack": false,
    """
    PATTERN = r'\"remove_occuring_attack\": .*?,'

    field_update(PATTERN, NEW_REMOVE_OCCURING, path)


def update_normalize_results(path):
    NEW_REMOVE_OCCURING = \
        r"""
        "normalize_results": false,
    """
    PATTERN = r'\"normalize_results\": .*?,'

    field_update(PATTERN, NEW_REMOVE_OCCURING, path)


def update_iterations(path):
    NEW_REMOVE_OCCURING = \
        r"""
        "attack_iterations": 2,
    """
    PATTERN = r'\"attack_iterations\": .*?,'

    field_update(PATTERN, NEW_REMOVE_OCCURING, path)


def update_early_stop(path):
    NEW_EARLY_STOP = \
        r"""
        "early_occuring_attack_stop": false
    """
    PATTERN = r'\"early_occuring_attack_stop\": .*?\s'

    field_update(PATTERN, NEW_EARLY_STOP, path)


def update_bounds(path):
    NEW_LOWER_BOUND = \
        r"""
        "niocs_lower_bound": 0,
    """
    NEW_UPPER_BOUND = \
        r"""
        "niocs_upper_bound": 50000,
    """
    PATTERN_LOWER = r'\"niocs_lower_bound\": .*?,'
    PATTERN_UPPER = r'\"niocs_upper_bound\": .*?,'

    field_update(PATTERN_LOWER, NEW_LOWER_BOUND, path)
    field_update(PATTERN_UPPER, NEW_UPPER_BOUND, path)


def update_create_results(path):
    NEW_CREATE_FN_FP = \
        r"""
        "create_fp_fn_results": true,
    """
    PATTERN_CREATE_FN_FP = r'\"create_fp_fn_results\": .*?,'

    NEW_CREATE_BUDGET = \
        r"""
        "create_budget_results": false,
    """
    PATTERN_CREATE_BUDGET = r'\"create_budget_results\": .*?,'

    NEW_CREATE_ATTACKS = \
        r"""
        "create_attacks_results": false,
    """
    PATTERN_CREATE_ATTACKS = r'\"create_attacks_results\": .*?,'

    NEW_CREATE_RANK = \
        r"""
        "create_rank_results": false,
    """
    PATTERN_CREATE_RANK = r'\"create_rank_results\": .*?,'

    field_update(PATTERN_CREATE_FN_FP, NEW_CREATE_FN_FP, path)
    field_update(PATTERN_CREATE_BUDGET, NEW_CREATE_BUDGET, path)
    field_update(PATTERN_CREATE_ATTACKS, NEW_CREATE_ATTACKS, path)
    field_update(PATTERN_CREATE_RANK, NEW_CREATE_RANK, path)


if __name__ == "__main__":
    for f in glob(FILES):
        update_fp_fn(f)
        update_attacks(f)
        update_thresholds(f)
        update_budgets(f)
        update_run_type(f)
        update_remove_occuring_attack(f)
        update_normalize_results(f)
        update_create_results(f)
        update_iterations(f)
        update_early_stop(f)
        update_bounds(f)
        print(f"Updated {f}")
